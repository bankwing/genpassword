package com.ibm.security;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class SecurityConfig {
    private static final String CONFIG_BUNDLE_NAME = "AA_Admin_Security";
    private static PropertyResourceBundle configBundleSecurity = null;
    private static final String AdministratorID = "AA.AdministratorID";
    private static final String ImplicitPermission = "AA.ImplicitPermission";
    private static final String PasswordLength = "AA.PasswordLength";
    private static final String PasswordUniqueTimes = "AA.PasswordUniqueTimes";
    private static final String PaswordForcedToChangeDays = "AA.PaswordForcedToChangeDays";
    private static final String ChangePasswordNotMoreThanDays = "AA.ChangePasswordNotMoreThanDays";
    private static final String SpecialCharacters = "AA.SpecialCharacters";
    private static final String InitialPasswordValidDays = "AA.InitialPasswordValidDays";
    private static final String SecurityKeyFile = "AA.SecurityKeyFile";
    private static final String CryptographyImplClass = "AA.CryptographyImplClass";
    private static final String PWDAlphaNumericCheck = "AA.PWDAlphaNumericCheck";
    private static final String PWDPrefixCheck = "AA.PWDPrefixCheck";
    private static final String AdminPasswordLength = "AA.AdminPasswordLength";
    private static final String AdminPWDUpperLowerCheck = "AA.AdminPWDUpperLowerCheck";

    public SecurityConfig() {
    }

    private static String getProperty(String key) {
        try {
            if (configBundleSecurity == null) {
                configBundleSecurity = (PropertyResourceBundle)ResourceBundle.getBundle("AA_Admin_Security");
            }

            return configBundleSecurity.getString(key);
        } catch (Exception var2) {
            System.out.println(var2);
            return null;
        }
    }

    public static String getAdministratorID() throws Exception {
        return getProperty("AA.AdministratorID");
    }

    public static String getChangePasswordNotMoreThanDays() throws Exception {
        return getProperty("AA.ChangePasswordNotMoreThanDays");
    }

    public static String getInitialPasswordValidDays() throws Exception {
        return getProperty("AA.InitialPasswordValidDays");
    }

    public static String getPasswordLength() throws Exception {
        return getProperty("AA.PasswordLength");
    }

    public static String getPasswordUniqueTimes() throws Exception {
        return getProperty("AA.PasswordUniqueTimes");
    }

    public static String getPaswordForcedToChangeDays() throws Exception {
        return getProperty("AA.PaswordForcedToChangeDays");
    }

    public static String getSpecialCharacters() throws Exception {
        return getProperty("AA.SpecialCharacters");
    }

    public static String getSecurityKeyFile() throws Exception {
        return getProperty("AA.SecurityKeyFile");
    }

    public static String getCryptographyImplClass() throws Exception {
        return getProperty("AA.CryptographyImplClass");
    }

    public static String getImplicitPermission() throws Exception {
        return getProperty("AA.ImplicitPermission");
    }

    public static String getPWDAlphaNumericCheck() throws Exception {
        return getProperty("AA.PWDAlphaNumericCheck") == null ? "Y" : getProperty("AA.PWDAlphaNumericCheck");
    }

    public static String getPWDPrefixCheck() throws Exception {
        return getProperty("AA.PWDPrefixCheck") == null ? "Y" : getProperty("AA.PWDPrefixCheck");
    }

    public static String getAdminPasswordLength() throws Exception {
        return getProperty("AA.AdminPasswordLength");
    }

    public static String getAdminPWDUpperLowerCheck() {
        return getProperty("AA.AdminPWDUpperLowerCheck") == null ? "Y" : getProperty("AA.AdminPWDUpperLowerCheck");
    }
}
