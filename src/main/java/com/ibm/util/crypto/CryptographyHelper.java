package com.ibm.util.crypto;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.lang.reflect.Constructor;

public class CryptographyHelper {
    private static ICryptography cryptographyHelper;

    public CryptographyHelper() {
    }

    public static ICryptography getInstance(String cryptographyImplClassName, String keyFileLocation) {
        try {
            if (cryptographyHelper == null) {
                try {
                    Class cl = Class.forName(cryptographyImplClassName);
                    Constructor[] ct = cl.getConstructors();
                    Object obj = ct[0].newInstance(keyFileLocation);
                    cryptographyHelper = (ICryptography)obj;
                } catch (Throwable var8) {
                    cryptographyHelper = null;
                }
            }
        } catch (Throwable var9) {
            cryptographyHelper = null;
        } finally {
            ;
        }

        return cryptographyHelper;
    }
}
