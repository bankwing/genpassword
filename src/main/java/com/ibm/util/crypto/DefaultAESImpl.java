package com.ibm.util.crypto;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Provider;
import java.security.Security;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class DefaultAESImpl implements ICryptography {
    private Cipher encryptCipher;
    private Cipher decryptCipher;
    private static final byte[] SALT = new byte[]{-87, -101, -56, 50, 86, 53, -29, 3, -88, -100, -55, 60, 87, 59, -18, 3};
    private static final String SPEC = "019283746565748392010192837465657483920101928374656574839201";
    private static final String UTF8_STRING = "UTF8";
    private static final String ALGORITHM = "AES";
    private String keyFile;

    public DefaultAESImpl(String kf) throws Exception {
        try {
            this.keyFile = kf;
            if (kf == null) {
                throw new Exception("key file location can not be null");
            } else {
                Provider[] ps = Security.getProviders();

                for(int i = 0; i < ps.length; ++i) {
                    System.out.println(ps[i].getInfo());
                    System.out.println(ps[i].getName());
                }

                KeyGenerator kgen = KeyGenerator.getInstance("AES");
                kgen.init(128);
                SecretKey skey = this.getPasswordKey();
                if (skey == null) {
                    skey = kgen.generateKey();
                }

                this.setPasswordKey(skey);
                byte[] raw = skey.getEncoded();
                SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
                this.encryptCipher = Cipher.getInstance("AES");
                this.decryptCipher = Cipher.getInstance("AES");
                this.encryptCipher.init(1, skeySpec);
                this.decryptCipher.init(2, skeySpec);
            }
        } catch (Throwable var7) {
            var7.printStackTrace();
            String preamble = "Exception caught while creating an instance of DefaultAESImpl: ";
            String msg = var7.getMessage();
            if (msg == null) {
                msg = var7.getClass().getName();
            }

            throw new Exception(preamble + msg);
        }
    }

    public String decrypt(String text) {
        try {
            byte[] dec = (new BASE64Decoder()).decodeBuffer(text);
            byte[] utf8 = this.decryptCipher.doFinal(dec);
            return new String(utf8, "UTF8");
        } catch (Exception var4) {
            var4.getMessage();
            var4.printStackTrace();
            return null;
        }
    }

    public String encrypt(String text) {
        try {
            byte[] utf8 = text.getBytes("UTF8");
            byte[] enc = this.encryptCipher.doFinal(utf8);
            return (new BASE64Encoder()).encode(enc);
        } catch (Exception var4) {
            var4.getMessage();
            var4.printStackTrace();
            return null;
        }
    }

    public SecretKey getPasswordKey() {
        SecretKey secretKey = null;

        try {
            System.out.println("Key file location: " + this.keyFile);
            String c = this.keyFile;
            File file = new File(c);
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(c);
                ObjectInputStream ois = new ObjectInputStream(fis);
                secretKey = (SecretKey)ois.readObject();
                ois.close();
                fis.close();
            } else {
                secretKey = null;
            }
        } catch (Throwable var8) {
            var8.printStackTrace();
            secretKey = null;
        } finally {
            ;
        }

        return secretKey;
    }

    public void setPasswordKey(SecretKey keySpec) {
        try {
            System.out.println("key file Loation: " + this.keyFile);
            String c = this.keyFile;
            FileOutputStream fos = new FileOutputStream(c);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(keySpec);
            oos.close();
            fos.close();
        } catch (Throwable var5) {
            var5.printStackTrace();
        }

    }

    public static String asHex(byte[] buf) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);

        for(int i = 0; i < buf.length; ++i) {
            if ((buf[i] & 255) < 16) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString((long)(buf[i] & 255), 16));
        }

        return strbuf.toString();
    }
}
