package com.ibm.util.crypto;

public interface ICryptography {
    String decrypt(String var1);

    String encrypt(String var1);
}
