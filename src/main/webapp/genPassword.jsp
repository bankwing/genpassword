<%@page import="com.ibm.security.*,com.ibm.util.crypto.*,com.ibm.util.*" %>
<b>Generate ICP-Inhouse Password</b><br><br><br>
<b>
<%
   String orginalString = request.getParameter("password");
   String actionKey = request.getParameter("actionKey");
   boolean encrypt = ((actionKey!=null)&&actionKey.equals("Encrypt"))?true:false;
   String encryptedString = "";
   String decryptedString = null;
   if(orginalString !=null){
	   			ICryptography crypto =
				CryptographyHelper.getInstance(
					SecurityConfig.getCryptographyImplClass(),
					SecurityConfig.getSecurityKeyFile());
			long start;
			long stop;
			if (encrypt) { // encrypt the original string
				start = System.currentTimeMillis();
				encryptedString = crypto.encrypt(orginalString);
				stop = System.currentTimeMillis();
				// print out results
				out.println("==========================================");
				out.println("<br>Original String  : " + orginalString);
				out.println("<br>Encrypted String : <font color='red'>" + encryptedString+"</font>");
			} else {
				// decrypt the original string
				start = System.currentTimeMillis();
				decryptedString = crypto.decrypt(orginalString);
				stop = System.currentTimeMillis();
				// print out results
				out.println("<br>==========================================");
				out.println("<br>Original String  : " + orginalString);
				out.println("<br>decrypted String : <font color='red'>" + decryptedString+"</font>");
			}
   }
%></b><br>

<form method="post">
	<input type="text" name="password">
	<input type="submit" value="Encrypt" name="actionKey">
	<input type="submit" value="Decrypt" name="actionKey">
	<input type="reset" value="reset">
</form>
